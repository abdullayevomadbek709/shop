

const Adminrow = document.getElementById("Adminrow");
const inputImg = document.getElementById("inputImg");
const inputTitle = document.getElementById("inputTitle");
const inputPrice = document.getElementById("inputPrice");
const EditButton = document.getElementById("editButton");
const form = document.getElementById("form");

let data = [];

async function getData() {
  let res = await fetch("http://localhost:3000/products");
  data = await res.json();
  render();
}

getData();

function render() {
  Adminrow.innerHTML = ""; // Sahifani tozalash

  data.forEach((item, index) => {
    Adminrow.innerHTML += `
      <div class="card  mb-5 shadow-lg p-3 mb-5 bg-body rounded p-2 d-flex align-center"  style="width: 18rem">
        <img src="${item.image}" class="card-img-top" width="80%"; height="300px"; alt="..." />
        <div class="card-body">
          <h5 class="card-title">Product shop</h5>
          <p class="card-text">${item.price}</p>
          <a href="#" class="btn btn-danger w-100 mb-3" onclick="editt(${index+1}, event)">Edit</a>
          <a href="#" class="btn btn-primary w-100" onclick="deleteBtn(${item.id})">Delete</a>
        </div>
      </div>
    `;
  });
}

function editt(id, event) {
  event.preventDefault();
  inputImg.value = data[id].image;
  inputTitle.value = data[id].title;
  inputPrice.value = data[id].price;
  put(id); 
}
function put(id) {
  EditButton.addEventListener("click", (e) => {
    e.stopImmediatePropagation();   
    e.preventDefault();
    e.stopImmediatePropagation();
    const updatedData = {
      image: inputImg.value.trim(),
      title: inputTitle.value.trim(),
      price: inputPrice.value.trim(),
    };
    fetch(`http://localhost:3000/products/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updatedData),
    })
      .then((response) => response.json())
      .then((updatedData) => {
        console.log(updatedData);
        getData();
        render();
      })
      .catch((error) => {
        alert(new Error("Hatolik mavjud API da"));
      });
  });
}

function deleteBtn(id) {
  fetch(`http://localhost:3000/products/${id}`, {
    method: "DELETE",
  });
}
