const row = document.getElementById("row");

let data = [];

async function getData() {
  let res = await fetch("http://localhost:3000/products");
  data = await res.json();
  render(data);
}

getData();

const basket = JSON.parse(localStorage.getItem("Basket")) || [];

function render(products) {
  products.map((item, index) => {
    row.innerHTML += `
      <div class="card mb-5 shadow-lg p-3 mb-5 bg-body rounded p-2" style="width: 18rem">
        <img src="${item.image}" class="card-img-top" width="80%" height="300px" alt="..." />
        <div class="card-body">
          <h5 class="card-title">Product shop</h5>
          <p class="card-text">${item.price}</p>
          <a href="#" class="btn btn-primary" onclick="addToBasket(${index}, event)">Basket</a>
        </div>
      </div>
    `;
  });
}

function addToBasket(id, event) {
  event.preventDefault();

  basket.push(data[id]);

  alert("Product added to the basket");
  localStorage.setItem("Basket", JSON.stringify(basket));
}
