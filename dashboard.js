let basketData = JSON.parse(localStorage.getItem("Basket"));

const basketRow = document.getElementById("BasketRow");

function render(products) {
  basketRow.innerHTML = ""; // Mahsulotlar ro'yxatini tozalash
  products.forEach((product, index) => {
    basketRow.innerHTML += `
      <div class="card mb-5 shadow-lg p-3 mb-5 bg-body rounded p-2" style="width: 18rem">
        <img src="${product.image}" class="card-img-top" width="80%" height="300px" alt="..." />
        <div class="card-body">
          <h5 class="card-title">Mahsulot do'koni</h5>
          <p class="card-text">${product.price}</p>
          <a href="#" class="btn btn-danger w-100" onclick="deleteProduct(${index})">O'chirish</a>
        </div>
      </div>
    `;
  });
}

render(basketData);

function deleteProduct(id) {
  if (id >= 0 && id < basketData.length) {
    basketData.splice(id, 1);

    localStorage.setItem("Basket", JSON.stringify(basketData));
    render(basketData);

    alert("Mahsulot o'chirildi");
  } else {
    alert("Notog'ri ID kiritildi");
  }
}
